import 'package:flutter/material.dart';

class Resposta extends StatelessWidget {
  var texto;
  Function func;

  Resposta({
    @required this.texto,
    @required this.func,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(
        left: 20,
        right: 20,
      ),
      child: RaisedButton(
        color: Colors.blue,
        textColor: Colors.white,
        child: Text(texto),
        onPressed: func,
      ),
    );
  }
}
