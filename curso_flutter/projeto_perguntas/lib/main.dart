import 'package:flutter/material.dart';
import 'package:projeto_perguntas/questionario.dart';
import 'package:projeto_perguntas/resultado.dart';

main() {
  runApp(PerguntaApp());
}

class _PerguntaAppState extends State<PerguntaApp> {
  int _perguntaCorrente = 0;
  int _pontuacaoTotal = 0;
  final _perguntas = const [
    {
      'texto': 'Qual a sua cor favorita?',
      'respostas': [
        {"texto": 'Preto', 'pontuacao': 10},
        {"texto": 'Vermelho', 'pontuacao': 5},
        {"texto": 'Verde', 'pontuacao': 3},
        {"texto": 'Branco', 'pontuacao': 1},
      ],
    },
    {
      'texto': 'Qual o seu animal favorito?',
      'respostas': [
        {"texto": 'Coelho', 'pontuacao': 10},
        {"texto": 'Cobra', 'pontuacao': 5},
        {"texto": 'Elefante', 'pontuacao': 3},
        {"texto": 'Leão', 'pontuacao': 1},
      ],
    },
    {
      'texto': 'Qual o seu instrutor favorito?',
      'respostas': [
        {"texto": 'Maria', 'pontuacao': 5},
        {"texto": 'João', 'pontuacao': 7},
        {"texto": 'Leo', 'pontuacao': 10},
        {"texto": 'Pedro', 'pontuacao': 3},
      ],
    }
  ];

  void _responder(int pontuacao) {
    if (temMaisPerguntas) {
      setState(() {
        _perguntaCorrente++;
        _pontuacaoTotal += pontuacao;
      });
    }
    // print(_pontuacaoTotal);
  }

  _reiniciarQuestionario() {
    setState(() {
      _perguntaCorrente = 0;
      _pontuacaoTotal = 0;
    });
  }

  bool get temMaisPerguntas {
    return _perguntaCorrente < _perguntas.length;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              "Barra",
            ),
          ),
        ),
        body: temMaisPerguntas
            ? Questionario(
                perguntas: _perguntas,
                perguntaSelecionada: _perguntaCorrente,
                quantoResponder: _responder,
              )
            : Resultado(
                _pontuacaoTotal,
                _reiniciarQuestionario,
              ),
      ),
    );
  }
}

class PerguntaApp extends StatefulWidget {
  _PerguntaAppState createState() {
    return _PerguntaAppState();
  }
}
