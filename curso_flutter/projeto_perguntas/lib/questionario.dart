import 'package:flutter/material.dart';
import 'package:projeto_perguntas/questao.dart';
import 'package:projeto_perguntas/resposta.dart';

class Questionario extends StatelessWidget {
  final List<Map<String, Object>> perguntas;
  final int perguntaSelecionada;
  final Function(int) quantoResponder;

  Questionario({
    @required this.perguntas,
    @required this.perguntaSelecionada,
    @required this.quantoResponder,
  });

  bool get temMaisPerguntas {
    return perguntaSelecionada < perguntas.length;
  }

  @override
  Widget build(BuildContext context) {
    // List<String> respostas = perguntas[_perguntaSelecionada]["respostas"];
    // List<Widget> widgets = [];
    // for (String textoResp in respostas) {
    //   widgets.add(
    //     Resposta(
    //       texto: textoResp,
    //       func: () => _responder(textoResp),
    //     ),
    //   );
    // }
    List<Map<String, Object>> respostas =
        temMaisPerguntas ? perguntas[perguntaSelecionada]["respostas"] : null;

    return Column(
      children: <Widget>[
        Questao(perguntas[perguntaSelecionada]["texto"]),
        ...respostas
            .map(
              (resp) => Resposta(
                texto: resp['texto'],
                func: () => quantoResponder(resp['pontuacao']),
              ),
            )
            .toList()
      ],
    );
  }
}
