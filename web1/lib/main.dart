import 'package:flutter/material.dart';
import 'package:web1/todo.dart';
import 'package:web1/containers.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Todo with Flutter', // Title da página
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: OpenContainerTransformDemo());
  }
}