
// ignore: unused_import
import 'package:intl/intl.dart' as intl;
import 'gallery_localizations.dart';

// ignore_for_file: unnecessary_brace_in_string_interps

/// The translations for Spanish Castilian (`es`).
class GalleryLocalizationsEs extends GalleryLocalizations {
  GalleryLocalizationsEs([String locale = 'es']) : super(locale);

  @override
  String githubRepo(Object repoName) {
    return 'Repositorio de GitHub ${repoName}';
  }

  @override
  String aboutDialogDescription(Object repoLink) {
    return 'Ve a {value} para ver el código fuente de esta aplicación.';
  }

  @override
  String get signIn => 'INICIAR SESIÓN';

  @override
  String get bannerDemoText => 'La contraseña se ha actualizado en el otro dispositivo. Vuelve a iniciar sesión.';

  @override
  String get bannerDemoResetText => 'Recuperar el banner';

  @override
  String get bannerDemoMultipleText => 'Varias acciones';

  @override
  String get bannerDemoLeadingText => 'Icono situado delante';

  @override
  String get dismiss => 'CERRAR';

  @override
  String get backToGallery => 'Volver a la galería';

  @override
  String get cardsDemoTappable => 'Se puede tocar';

  @override
  String get cardsDemoSelectable => 'Se puede seleccionar (mantener pulsado)';

  @override
  String get cardsDemoExplore => 'Explorar';

  @override
  String cardsDemoExploreSemantics(Object destinationName) {
    return 'Explorar ${destinationName}';
  }

  @override
  String cardsDemoShareSemantics(Object destinationName) {
    return 'Compartir ${destinationName}';
  }

  @override
  String get cardsDemoTravelDestinationTitle1 => 'Las 10 ciudades que debes visitar en Tamil Nadu';

  @override
  String get cardsDemoTravelDestinationDescription1 => 'Número 10';

  @override
  String get cardsDemoTravelDestinationCity1 => 'Thanjavur';

  @override
  String get cardsDemoTravelDestinationLocation1 => 'Thanjavur (Tamil Nadu)';

  @override
  String get cardsDemoTravelDestinationTitle2 => 'Artesanos del sur de India';

  @override
  String get cardsDemoTravelDestinationDescription2 => 'Hilanderas de seda';

  @override
  String get cardsDemoTravelDestinationCity2 => 'Chettinad';

  @override
  String get cardsDemoTravelDestinationLocation2 => 'Sivaganga (Tamil Nadu)';

  @override
  String get cardsDemoTravelDestinationTitle3 => 'Templo Brihadisvara';

  @override
  String get cardsDemoTravelDestinationDescription3 => 'Templos';

  @override
  String get homeHeaderGallery => 'Galería';

  @override
  String get homeHeaderCategories => 'Categorías';

  @override
  String get shrineDescription => 'Una aplicación para comprar productos de moda';

  @override
  String get fortnightlyDescription => 'Aplicación de noticias centrada en la calidad de su contenido';

  @override
  String get rallyDescription => 'Una aplicación de finanzas personales';

  @override
  String get replyDescription => 'An efficient, focused email app';

  @override
  String get rallyAccountDataChecking => 'Cuenta corriente';

  @override
  String get rallyAccountDataHomeSavings => 'Ahorros para la casa';

  @override
  String get rallyAccountDataCarSavings => 'Ahorros para el coche';

  @override
  String get rallyAccountDataVacation => 'Vacaciones';

  @override
  String get rallyAccountDetailDataAnnualPercentageYield => 'Porcentaje de rendimiento anual';

  @override
  String get rallyAccountDetailDataInterestRate => 'Tipo de interés';

  @override
  String get rallyAccountDetailDataInterestYtd => 'Intereses pagados este año hasta la fecha';

  @override
  String get rallyAccountDetailDataInterestPaidLastYear => 'Intereses pagados el año pasado';

  @override
  String get rallyAccountDetailDataNextStatement => 'Siguiente extracto';

  @override
  String get rallyAccountDetailDataAccountOwner => 'Propietario de la cuenta';

  @override
  String get rallyBillDetailTotalAmount => 'Importe total';

  @override
  String get rallyBillDetailAmountPaid => 'Importe pagado';

  @override
  String get rallyBillDetailAmountDue => 'Importe a pagar';

  @override
  String get rallyBudgetCategoryCoffeeShops => 'Cafeterías';

  @override
  String get rallyBudgetCategoryGroceries => 'Supermercados';

  @override
  String get rallyBudgetCategoryRestaurants => 'Restaurantes';

  @override
  String get rallyBudgetCategoryClothing => 'Ropa';

  @override
  String get rallyBudgetDetailTotalCap => 'Límite presupuestario';

  @override
  String get rallyBudgetDetailAmountUsed => 'Importe gastado';

  @override
  String get rallyBudgetDetailAmountLeft => 'Importe restante';

  @override
  String get rallySettingsManageAccounts => 'Gestionar cuentas';

  @override
  String get rallySettingsTaxDocuments => 'Documentos fiscales';

  @override
  String get rallySettingsPasscodeAndTouchId => 'Contraseña y Touch ID';

  @override
  String get rallySettingsNotifications => 'Notificaciones';

  @override
  String get rallySettingsPersonalInformation => 'Información personal';

  @override
  String get rallySettingsPaperlessSettings => 'Ajustes sin papel';

  @override
  String get rallySettingsFindAtms => 'Buscar cajeros automáticos';

  @override
  String get rallySettingsHelp => 'Ayuda';

  @override
  String get rallySettingsSignOut => 'Cerrar sesión';

  @override
  String get rallyAccountTotal => 'Total';

  @override
  String get rallyBillsDue => 'Pendiente:';

  @override
  String get rallyBudgetLeft => 'Presupuesto restante:';

  @override
  String get rallyAccounts => 'Cuentas';

  @override
  String get rallyBills => 'Facturas';

  @override
  String get rallyBudgets => 'Presupuestos';

  @override
  String get rallyAlerts => 'Alertas';

  @override
  String get rallySeeAll => 'VER TODO';

  @override
  String get rallyFinanceLeft => 'RESTANTE';

  @override
  String get rallyTitleOverview => 'RESUMEN';

  @override
  String get rallyTitleAccounts => 'CUENTAS';

  @override
  String get rallyTitleBills => 'FACTURAS';

  @override
  String get rallyTitleBudgets => 'PRESUPUESTOS';

  @override
  String get rallyTitleSettings => 'AJUSTES';

  @override
  String get rallyLoginLoginToRally => 'Iniciar sesión en Rally';

  @override
  String get rallyLoginNoAccount => '¿No tienes una cuenta?';

  @override
  String get rallyLoginSignUp => 'REGISTRARSE';

  @override
  String get rallyLoginUsername => 'Nombre de usuario';

  @override
  String get rallyLoginPassword => 'Contraseña';

  @override
  String get rallyLoginLabelLogin => 'Iniciar sesión';

  @override
  String get rallyLoginRememberMe => 'Recordarme';

  @override
  String get rallyLoginButtonLogin => 'INICIAR SESIÓN';

  @override
  String rallyAlertsMessageHeadsUpShopping(Object percent) {
    return 'Aviso: Has utilizado un ${percent} de tu presupuesto para compras este mes.';
  }

  @override
  String rallyAlertsMessageSpentOnRestaurants(Object amount) {
    return 'Has gastado ${amount} en restaurantes esta semana.';
  }

  @override
  String rallyAlertsMessageATMFees(Object amount) {
    return 'Has pagado ${amount} de comisiones por utilizar cajeros automáticos este mes.';
  }

  @override
  String rallyAlertsMessageCheckingAccount(Object percent) {
    return '¡Bien hecho! El saldo positivo de tu cuenta corriente está un ${percent} más alto que el mes pasado.';
  }

  @override
  String rallyAlertsMessageUnassignedTransactions(int count) {
    return intl.Intl.pluralLogic(
      count,
      locale: localeName,
      one: 'Aumenta tu posible deducción fiscal Asigna categorías a 1 transacción sin asignar.',
      other: 'Aumenta tu posible deducción fiscal Asigna categorías a ${count} transacciones sin asignar.',
    );
  }

  @override
  String get rallySeeAllAccounts => 'Ver todas las cuentas';

  @override
  String get rallySeeAllBills => 'Ver todas las facturas';

  @override
  String get rallySeeAllBudgets => 'Ver todos los presupuestos';

  @override
  String rallyAccountAmount(Object accountName, Object accountNumber, Object amount) {
    return 'Cuenta ${accountName} (${accountNumber}) con ${amount}.';
  }

  @override
  String rallyBillAmount(Object billName, Object date, Object amount) {
    return 'Fecha límite de la factura ${billName} (${amount}): ${date}.';
  }

  @override
  String rallyBudgetAmount(Object budgetName, Object amountUsed, Object amountTotal, Object amountLeft) {
    return 'Has gastado ${amountUsed} de ${amountTotal} del presupuesto ${budgetName}. Cantidad restante: ${amountLeft}';
  }

  @override
  String get craneDescription => 'Una aplicación de viajes personalizada';

  @override
  String get homeCategoryReference => 'ESTILOS Y OTROS';

  @override
  String get demoInvalidURL => 'No se ha podido mostrar la URL:';

  @override
  String get demoOptionsTooltip => 'Opciones';

  @override
  String get demoInfoTooltip => 'Información';

  @override
  String get demoCodeTooltip => 'Código de demostración';

  @override
  String get demoDocumentationTooltip => 'Documentación de la API';

  @override
  String get demoFullscreenTooltip => 'Pantalla completa';

  @override
  String get demoCodeViewerCopyAll => 'COPIAR TODO';

  @override
  String get demoCodeViewerCopiedToClipboardMessage => 'Se ha copiado en el portapapeles.';

  @override
  String demoCodeViewerFailedToCopyToClipboardMessage(Object error) {
    return 'No se ha podido copiar en el portapapeles: ${error}';
  }

  @override
  String get demoOptionsFeatureTitle => 'Ver opciones';

  @override
  String get demoOptionsFeatureDescription => 'Toca aquí para ver las opciones disponibles en esta demostración.';

  @override
  String get settingsTitle => 'Ajustes';

  @override
  String get settingsButtonLabel => 'Ajustes';

  @override
  String get settingsButtonCloseLabel => 'Cerrar la configuración';

  @override
  String get settingsSystemDefault => 'Sistema';

  @override
  String get settingsTextScaling => 'Ajuste de texto';

  @override
  String get settingsTextScalingSmall => 'Pequeño';

  @override
  String get settingsTextScalingNormal => 'Normal';

  @override
  String get settingsTextScalingLarge => 'Grande';

  @override
  String get settingsTextScalingHuge => 'Enorme';

  @override
  String get settingsTextDirection => 'Dirección del texto';

  @override
  String get settingsTextDirectionLocaleBased => 'Basado en la configuración regional';

  @override
  String get settingsTextDirectionLTR => 'Texto de izquierda a derecha';

  @override
  String get settingsTextDirectionRTL => 'Texto de derecha a izquierda';

  @override
  String get settingsLocale => 'Configuración regional';

  @override
  String get settingsPlatformMechanics => 'Mecánica de la plataforma';

  @override
  String get settingsTheme => 'Tema';

  @override
  String get settingsDarkTheme => 'Oscuro';

  @override
  String get settingsLightTheme => 'Claro';

  @override
  String get settingsSlowMotion => 'Cámara lenta';

  @override
  String get settingsAbout => 'Acerca de Flutter Gallery';

  @override
  String get settingsFeedback => 'Enviar comentarios';

  @override
  String get settingsAttribution => 'Diseñado por TOASTER en Londres';

  @override
  String get demoBottomAppBarTitle => 'Barra de aplicaciones inferior';

  @override
  String get demoBottomAppBarSubtitle => 'Muestra la navegación y las acciones en la parte inferior';

  @override
  String get demoBottomAppBarDescription => 'Las barras de aplicaciones inferiores proporcionan acceso a un panel de navegación inferior y a hasta cuatro acciones, incluido el botón de acción flotante.';

  @override
  String get bottomAppBarNotch => 'Recorte';

  @override
  String get bottomAppBarPosition => 'Posición del botón de acción flotante';

  @override
  String get bottomAppBarPositionDockedEnd => 'Acoplado a la base: parte inferior';

  @override
  String get bottomAppBarPositionDockedCenter => 'Acoplado a la base: parte central';

  @override
  String get bottomAppBarPositionFloatingEnd => 'Flotante: parte inferior';

  @override
  String get bottomAppBarPositionFloatingCenter => 'Flotante: parte central';

  @override
  String get demoBannerTitle => 'Banner';

  @override
  String get demoBannerSubtitle => 'Se muestra un banner en una lista';

  @override
  String get demoBannerDescription => 'Un banner contiene un breve mensaje importante y muestra a los usuarios acciones que deben tomar, aunque puede que solo tengan que cerrar el banner. Para poder cerrarlo, el usuario debe hacer algo antes.';

  @override
  String get demoBottomNavigationTitle => 'Navegación inferior';

  @override
  String get demoBottomNavigationSubtitle => 'Navegación inferior con vistas de fusión cruzada';

  @override
  String get demoBottomNavigationPersistentLabels => 'Etiquetas persistentes';

  @override
  String get demoBottomNavigationSelectedLabel => 'Etiqueta seleccionada';

  @override
  String get demoBottomNavigationDescription => 'En la barra de navegación inferior de la pantalla se muestran entre tres y cinco destinos. Cada destino está representado mediante un icono y, de forma opcional, con una etiqueta de texto. Al tocar un icono de navegación inferior, se redirige al usuario al destino de nivel superior que esté asociado a ese icono.';

  @override
  String get demoButtonTitle => 'Botones';

  @override
  String get demoButtonSubtitle => 'Plano, con relieve, con contorno y más';

  @override
  String get demoFlatButtonTitle => 'Botón plano';

  @override
  String get demoFlatButtonDescription => 'Al pulsar un botón plano, se muestra una salpicadura de tinta que no recupera el relieve al dejar de pulsarse. Utiliza este tipo de botones en barras de herramientas, cuadros de diálogo y elementos insertados con márgenes.';

  @override
  String get demoRaisedButtonTitle => 'Botón con relieve';

  @override
  String get demoRaisedButtonDescription => 'Los botones con relieve añaden dimensión a los diseños mayormente planos. Destacan funciones en espacios llenos o amplios.';

  @override
  String get demoOutlineButtonTitle => 'Botón con contorno';

  @override
  String get demoOutlineButtonDescription => 'Los botones con contorno se vuelven opacos y se elevan al pulsarlos. Suelen aparecer junto a botones elevados para indicar una acción secundaria alternativa.';

  @override
  String get demoToggleButtonTitle => 'Botones que se pueden activar y desactivar';

  @override
  String get demoToggleButtonDescription => 'Se pueden usar los botones de activar y desactivar para agrupar opciones relacionadas. Para destacar grupos de botones que se pueden activar y desactivar relacionados, los botones deben compartir un mismo contenedor';

  @override
  String get demoFloatingButtonTitle => 'Botón de acción flotante';

  @override
  String get demoFloatingButtonDescription => 'Un botón de acción flotante es un botón con un icono circular que aparece sobre contenido para fomentar una acción principal en la aplicación.';

  @override
  String get demoCardTitle => 'Tarjetas';

  @override
  String get demoCardSubtitle => 'Tarjetas de referencia con esquinas redondeadas';

  @override
  String get demoChipTitle => 'Chips';

  @override
  String get demoCardDescription => 'Una tarjeta es una hoja de Material utilizada para representar información relacionada, como un álbum, una ubicación geográfica, una comida, datos de contacto, etc.';

  @override
  String get demoChipSubtitle => 'Elementos compactos que representan atributos, acciones o texto que se ha introducido';

  @override
  String get demoActionChipTitle => 'Chip de acción';

  @override
  String get demoActionChipDescription => 'Los chips de acción son un conjunto de opciones que permiten llevar a cabo tareas relacionadas con el contenido principal. Deberían aparecer de forma dinámica y según el contexto en la interfaz.';

  @override
  String get demoChoiceChipTitle => 'Chip de elección';

  @override
  String get demoChoiceChipDescription => 'Los chips de elección representan una opción de un conjunto de opciones. Incluyen descripciones o categorías relacionadas.';

  @override
  String get demoFilterChipTitle => 'Chip de filtro';

  @override
  String get demoFilterChipDescription => 'Los chips de filtro sirven para filtrar contenido por etiquetas o palabras descriptivas.';

  @override
  String get demoInputChipTitle => 'Chip de entrada';

  @override
  String get demoInputChipDescription => 'Los chips de entrada representan datos complejos de forma compacta, como textos o entidades (por ejemplo, personas, lugares o cosas).';

  @override
  String get demoDataTableTitle => 'Tablas de datos';

  @override
  String get demoDataTableSubtitle => 'Filas y columnas de información';

  @override
  String get demoDataTableDescription => 'Las tablas de datos muestran información en un formato de cuadrícula estructurado en filas y columnas. Organizan la información de manera que sea fácil de analizar para que los usuarios puedan detectar patrones y datos interesantes.';

  @override
  String get dataTableHeader => 'Nutrición';

  @override
  String get dataTableColumnDessert => 'Postre (1 ración)';

  @override
  String get dataTableColumnCalories => 'Calorías';

  @override
  String get dataTableColumnFat => 'Grasas (g)';

  @override
  String get dataTableColumnCarbs => 'Carbohidratos (g)';

  @override
  String get dataTableColumnProtein => 'Proteínas (g)';

  @override
  String get dataTableColumnSodium => 'Sodio (mg)';

  @override
  String get dataTableColumnCalcium => 'Calcio (%)';

  @override
  String get dataTableColumnIron => 'Hierro (%)';

  @override
  String get dataTableRowFrozenYogurt => 'Frozen yogurt';

  @override
  String get dataTableRowIceCreamSandwich => 'Ice cream sandwich';

  @override
  String get dataTableRowEclair => 'Eclair';

  @override
  String get dataTableRowCupcake => 'Cupcake';

  @override
  String get dataTableRowGingerbread => 'Gingerbread';

  @override
  String get dataTableRowJellyBean => 'Jelly bean';

  @override
  String get dataTableRowLollipop => 'Lollipop';

  @override
  String get dataTableRowHoneycomb => 'Honeycomb';

  @override
  String get dataTableRowDonut => 'Donut';

  @override
  String get dataTableRowApplePie => 'Apple pie';

  @override
  String dataTableRowWithSugar(Object value) {
    return '${value} con azúcar';
  }

  @override
  String dataTableRowWithHoney(Object value) {
    return '${value} con miel';
  }

  @override
  String get demoDialogTitle => 'Cuadros de diálogo';

  @override
  String get demoDialogSubtitle => 'Sencillo, con alerta y a pantalla completa';

  @override
  String get demoAlertDialogTitle => 'Con alerta';

  @override
  String get demoAlertDialogDescription => 'En un cuadro de diálogo de alerta se informa al usuario sobre situaciones que requieren su confirmación. Este tipo de cuadros de diálogo incluyen un título opcional y una lista de acciones opcional.';

  @override
  String get demoAlertTitleDialogTitle => 'Alerta con título';

  @override
  String get demoSimpleDialogTitle => 'Sencillo';

  @override
  String get demoSimpleDialogDescription => 'Un cuadro de diálogo sencillo ofrece al usuario la posibilidad de elegir entre diversas opciones e incluye un título opcional que se muestra encima de las opciones.';

  @override
  String get demoGridListsTitle => 'Listas con cuadrícula';

  @override
  String get demoGridListsSubtitle => 'Diseño de fila y columna';

  @override
  String get demoGridListsDescription => 'Las listas con cuadrícula son las más adecuadas para presentar datos homogéneos, normalmente imágenes. Los elementos de las listas con cuadrícula se llaman mosaicos.';

  @override
  String get demoGridListsImageOnlyTitle => 'Solo imagen';

  @override
  String get demoGridListsHeaderTitle => 'Con encabezado';

  @override
  String get demoGridListsFooterTitle => 'Con pie de página';

  @override
  String get demoSlidersTitle => 'Controles deslizantes';

  @override
  String get demoSlidersSubtitle => 'Widgets para seleccionar un valor deslizando el dedo';

  @override
  String get demoSlidersDescription => 'Los controles deslizantes muestran un intervalo de valores en una barra, de la que los usuarios pueden seleccionar un único valor. Pueden resultar muy útiles para modificar ajustes como el volumen o el brillo o aplicar filtros de imagen.';

  @override
  String get demoRangeSlidersTitle => 'Controles deslizantes de intervalos';

  @override
  String get demoRangeSlidersDescription => 'Los controles deslizantes muestran un intervalo de valores en una barra, que puede tener iconos en ambos extremos. Pueden resultar muy útiles para modificar ajustes como el volumen o el brillo o aplicar filtros de imagen.';

  @override
  String get demoCustomSlidersTitle => 'Controles deslizantes personalizados';

  @override
  String get demoCustomSlidersDescription => 'Los controles deslizantes muestran un intervalo de valores en una barra, de la cual los usuarios pueden seleccionar un solo valor o intervalo. Se pueden aplicar temas a los controles deslizantes y personalizarlos.';

  @override
  String get demoSlidersContinuousWithEditableNumericalValue => 'Valores continuos con un valor numérico editable';

  @override
  String get demoSlidersDiscrete => 'Valores discretos';

  @override
  String get demoSlidersDiscreteSliderWithCustomTheme => 'Control deslizante de valores discretos con un tema personalizado';

  @override
  String get demoSlidersContinuousRangeSliderWithCustomTheme => 'Control deslizante de intervalo y valores continuos con un tema personalizado';

  @override
  String get demoSlidersContinuous => 'Valores continuos';

  @override
  String get demoSlidersEditableNumericalValue => 'Valor numérico editable';

  @override
  String get demoMenuTitle => 'Menú';

  @override
  String get demoContextMenuTitle => 'Menú contextual';

  @override
  String get demoSectionedMenuTitle => 'Menú con secciones';

  @override
  String get demoSimpleMenuTitle => 'Menú sencillo';

  @override
  String get demoChecklistMenuTitle => 'Menú con lista de comprobación';

  @override
  String get demoMenuSubtitle => 'Botones del menú y menús sencillos';

  @override
  String get demoMenuDescription => 'Un menú muestra una lista de opciones en una superficie temporal. Se muestran cuando los usuarios interaccionan con un botón, una acción u otro control.';

  @override
  String get demoMenuItemValueOne => 'Opción de menú uno';

  @override
  String get demoMenuItemValueTwo => 'Opción de menú dos';

  @override
  String get demoMenuItemValueThree => 'Opción de menú tres';

  @override
  String get demoMenuOne => 'Uno';

  @override
  String get demoMenuTwo => 'Dos';

  @override
  String get demoMenuThree => 'Tres';

  @override
  String get demoMenuFour => 'Cuatro';

  @override
  String get demoMenuAnItemWithAContextMenuButton => 'Una opción con un menú contextual';

  @override
  String get demoMenuContextMenuItemOne => 'Opción de menú contextual uno';

  @override
  String get demoMenuADisabledMenuItem => 'Opción de menú inhabilitada';

  @override
  String get demoMenuContextMenuItemThree => 'Opción de menú contextual tres';

  @override
  String get demoMenuAnItemWithASectionedMenu => 'Una opción con un menú con secciones';

  @override
  String get demoMenuPreview => 'Vista previa';

  @override
  String get demoMenuShare => 'Compartir';

  @override
  String get demoMenuGetLink => 'Obtener enlace';

  @override
  String get demoMenuRemove => 'Quitar';

  @override
  String demoMenuSelected(Object value) {
    return 'Seleccionado: ${value}';
  }

  @override
  String demoMenuChecked(Object value) {
    return 'Marcado: ${value}';
  }

  @override
  String get demoMenuAnItemWithASimpleMenu => 'Una opción con un menú sencillo';

  @override
  String get demoMenuAnItemWithAChecklistMenu => 'Una opción con un menú con lista de comprobación';

  @override
  String get demoFullscreenDialogTitle => 'A pantalla completa';

  @override
  String get demoFullscreenDialogDescription => 'La propiedad fullscreenDialog especifica si la página entrante es un cuadro de diálogo modal a pantalla completa';

  @override
  String get demoCupertinoActivityIndicatorTitle => 'Indicador de actividad';

  @override
  String get demoCupertinoActivityIndicatorSubtitle => 'Indicadores de actividad similares a los de iOS';

  @override
  String get demoCupertinoActivityIndicatorDescription => 'Un indicador de actividad similar al de iOS que gira en el sentido de las agujas del reloj.';

  @override
  String get demoCupertinoButtonsTitle => 'Botones';

  @override
  String get demoCupertinoButtonsSubtitle => 'Botones similares a los de iOS';

  @override
  String get demoCupertinoButtonsDescription => 'Un botón similar a los de iOS que incluye texto o un icono que desaparece y aparece al tocarlo. Puede tener un fondo opcionalmente.';

  @override
  String get demoCupertinoAlertsTitle => 'Alertas';

  @override
  String get demoCupertinoAlertsSubtitle => 'Cuadros de diálogo de alerta similares a los de iOS';

  @override
  String get demoCupertinoAlertTitle => 'Alerta';

  @override
  String get demoCupertinoAlertDescription => 'En un cuadro de diálogo de alerta se informa al usuario sobre situaciones que requieren su confirmación. Un cuadro de diálogo de alerta incluye un título opcional, contenido opcional y una lista con acciones opcional. El título se muestra encima del contenido y las acciones, bajo el contenido.';

  @override
  String get demoCupertinoAlertWithTitleTitle => 'Alerta con título';

  @override
  String get demoCupertinoAlertButtonsTitle => 'Alerta con botones';

  @override
  String get demoCupertinoAlertButtonsOnlyTitle => 'Solo botones de alerta';

  @override
  String get demoCupertinoActionSheetTitle => 'Hoja de acción';

  @override
  String get demoCupertinoActionSheetDescription => 'Una hoja de acción es un estilo concreto de alerta que presenta al usuario dos o más opciones relacionadas con el contexto; puede incluir un título, un mensaje adicional y una lista con acciones.';

  @override
  String get demoCupertinoNavigationBarTitle => 'Barra de navegación';

  @override
  String get demoCupertinoNavigationBarSubtitle => 'Barra de navegación similar a la de iOS';

  @override
  String get demoCupertinoNavigationBarDescription => 'Una barra de navegación similar a la de iOS. La barra de navegación es una barra de herramientas que básicamente contiene el título de una página en medio de la barra.';

  @override
  String get demoCupertinoPickerTitle => 'Selectores';

  @override
  String get demoCupertinoPickerSubtitle => 'Selectores de fecha y hora similares a los de iOS';

  @override
  String get demoCupertinoPickerDescription => 'Widget de un selector similar a los de iOS desde el que se puede seleccionar fechas, horas o ambos valores.';

  @override
  String get demoCupertinoPickerTimer => 'Temporizador';

  @override
  String get demoCupertinoPickerDate => 'Fecha';

  @override
  String get demoCupertinoPickerTime => 'Hora';

  @override
  String get demoCupertinoPickerDateTime => 'Fecha y hora';

  @override
  String get demoCupertinoPullToRefreshTitle => 'Desliza hacia abajo para actualizar';

  @override
  String get demoCupertinoPullToRefreshSubtitle => 'Control con la función \"Desliza hacia abajo para actualizar\" similar al de iOS';

  @override
  String get demoCupertinoPullToRefreshDescription => 'Un widget que implementa el control de contenido con la función \"Desliza hacia abajo para actualizar\" similar al de iOS.';

  @override
  String get demoCupertinoSegmentedControlTitle => 'Control segmentado';

  @override
  String get demoCupertinoSegmentedControlSubtitle => 'Control segmentado similar al de iOS';

  @override
  String get demoCupertinoSegmentedControlDescription => 'Se usa para seleccionar entre un número de opciones igualmente exclusivas. Si se selecciona una opción en el control segmentado, el resto no se podrán seleccionar.';

  @override
  String get demoCupertinoSliderTitle => 'Control deslizante';

  @override
  String get demoCupertinoSliderSubtitle => 'Control deslizante similar al de iOS';

  @override
  String get demoCupertinoSliderDescription => 'Los controles deslizantes sirven para seleccionar valores de un conjunto de valores continuos o específicos.';

  @override
  String demoCupertinoSliderContinuous(Object value) {
    return 'Continuo: ${value}';
  }

  @override
  String demoCupertinoSliderDiscrete(Object value) {
    return 'Específico: ${value}';
  }

  @override
  String get demoCupertinoSwitchSubtitle => 'Interruptor similar al de iOS';

  @override
  String get demoCupertinoSwitchDescription => 'Los interruptores sirven para activar o desactivar un solo ajuste.';

  @override
  String get demoCupertinoTabBarTitle => 'Barra de pestañas';

  @override
  String get demoCupertinoTabBarSubtitle => 'Barra de pestañas de la parte inferior similar a la de iOS';

  @override
  String get demoCupertinoTabBarDescription => 'Una barra de pestañas de navegación situada en la parte inferior similar a la de iOS. Muestra varias pestañas, con una de ellas activa, que de forma predeterminada es la primera.';

  @override
  String get cupertinoTabBarHomeTab => 'Página principal';

  @override
  String get cupertinoTabBarChatTab => 'Chat';

  @override
  String get cupertinoTabBarProfileTab => 'Perfil';

  @override
  String get demoCupertinoTextFieldTitle => 'Campos de texto';

  @override
  String get demoCupertinoTextFieldSubtitle => 'Campos de texto similares a los de iOS';

  @override
  String get demoCupertinoTextFieldDescription => 'Un campo de texto permite que el usuario introduzca texto, tanto mediante el teclado del hardware como mediante el teclado en pantalla.';

  @override
  String get demoCupertinoTextFieldPIN => 'PIN';

  @override
  String get demoMotionTitle => 'Movimiento';

  @override
  String get demoMotionSubtitle => 'Todos los patrones de transición predefinidos';

  @override
  String get demoContainerTransformDemoInstructions => 'Cards, Lists & FAB';

  @override
  String get demoSharedXAxisDemoInstructions => 'Next and Back Buttons';

  @override
  String get demoSharedYAxisDemoInstructions => 'Sort by \"Recently Played\"';

  @override
  String get demoSharedZAxisDemoInstructions => 'Settings icon button';

  @override
  String get demoFadeThroughDemoInstructions => 'Bottom navigation';

  @override
  String get demoFadeScaleDemoInstructions => 'Modal and FAB';

  @override
  String get demoContainerTransformTitle => 'Transformación de contenedores';

  @override
  String get demoContainerTransformDescription => 'El patrón \"Transformación de contenedores\" está diseñado para usarse en transiciones entre elementos de la interfaz que incluyen un contenedor. Este patrón crea una conexión visible entre 2 elementos de la interfaz.';

  @override
  String get demoContainerTransformModalBottomSheetTitle => 'Modo de atenuación';

  @override
  String get demoContainerTransformTypeFade => 'ATENUAR';

  @override
  String get demoContainerTransformTypeFadeThrough => 'CAMBIAR CON ATENUACIÓN';

  @override
  String get demoMotionPlaceholderTitle => 'Título';

  @override
  String get demoMotionPlaceholderSubtitle => 'Texto secundario';

  @override
  String get demoMotionSmallPlaceholderSubtitle => 'Elementos secundarios';

  @override
  String get demoMotionDetailsPageTitle => 'Página de detalles';

  @override
  String get demoMotionListTileTitle => 'Elemento de lista';

  @override
  String get demoSharedAxisDescription => 'El patrón \"Eje compartido\" se utiliza en transiciones entre elementos de la interfaz que tienen una relación espacial o de navegación. Este patrón hace una transición basándose en el eje X, Y o Z para reforzar la relación que hay entre los elementos.';

  @override
  String get demoSharedXAxisTitle => 'Eje X compartido';

  @override
  String get demoSharedXAxisBackButtonText => 'ATRÁS';

  @override
  String get demoSharedXAxisNextButtonText => 'SIGUIENTE';

  @override
  String get demoSharedXAxisCoursePageTitle => 'Optimiza tus cursos';

  @override
  String get demoSharedXAxisCoursePageSubtitle => 'Las categorías agrupadas aparecen como grupos en tu feed. Puedes cambiarlo en cualquier momento.';

  @override
  String get demoSharedXAxisArtsAndCraftsCourseTitle => 'Arte y artesanía';

  @override
  String get demoSharedXAxisBusinessCourseTitle => 'Empresa';

  @override
  String get demoSharedXAxisIllustrationCourseTitle => 'Ilustración';

  @override
  String get demoSharedXAxisDesignCourseTitle => 'Diseño';

  @override
  String get demoSharedXAxisCulinaryCourseTitle => 'Gastronomía';

  @override
  String get demoSharedXAxisBundledCourseSubtitle => 'Agrupado';

  @override
  String get demoSharedXAxisIndividualCourseSubtitle => 'Por separado';

  @override
  String get demoSharedXAxisSignInWelcomeText => 'Hola, David Park';

  @override
  String get demoSharedXAxisSignInSubtitleText => 'Inicia sesión con tu cuenta';

  @override
  String get demoSharedXAxisSignInTextFieldLabel => 'Correo electrónico o número de teléfono';

  @override
  String get demoSharedXAxisForgotEmailButtonText => '¿HAS OLVIDADO TU CORREO ELECTRÓNICO?';

  @override
  String get demoSharedXAxisCreateAccountButtonText => 'CREAR CUENTA';

  @override
  String get demoSharedYAxisTitle => 'Eje Y compartido';

  @override
  String get demoSharedYAxisAlbumCount => '268 álbumes';

  @override
  String get demoSharedYAxisAlphabeticalSortTitle => 'A-Z';

  @override
  String get demoSharedYAxisRecentSortTitle => 'Reproducido recientemente';

  @override
  String get demoSharedYAxisAlbumTileTitle => 'Álbum';

  @override
  String get demoSharedYAxisAlbumTileSubtitle => 'Artista';

  @override
  String get demoSharedYAxisAlbumTileDurationUnit => 'min';

  @override
  String get demoSharedZAxisTitle => 'Eje Z compartido';

  @override
  String get demoSharedZAxisSettingsPageTitle => 'Ajustes';

  @override
  String get demoSharedZAxisBurgerRecipeTitle => 'Hamburguesa';

  @override
  String get demoSharedZAxisBurgerRecipeDescription => 'Receta de hamburguesa';

  @override
  String get demoSharedZAxisSandwichRecipeTitle => 'Bocadillo';

  @override
  String get demoSharedZAxisSandwichRecipeDescription => 'Receta de bocadillo';

  @override
  String get demoSharedZAxisDessertRecipeTitle => 'Postre';

  @override
  String get demoSharedZAxisDessertRecipeDescription => 'Receta de postre';

  @override
  String get demoSharedZAxisShrimpPlateRecipeTitle => 'Gamba';

  @override
  String get demoSharedZAxisShrimpPlateRecipeDescription => 'Receta de plato de gambas';

  @override
  String get demoSharedZAxisCrabPlateRecipeTitle => 'Cangrejo';

  @override
  String get demoSharedZAxisCrabPlateRecipeDescription => 'Receta de plato de cangrejo';

  @override
  String get demoSharedZAxisBeefSandwichRecipeTitle => 'Bocadillo de ternera';

  @override
  String get demoSharedZAxisBeefSandwichRecipeDescription => 'Receta de bocadillo de ternera';

  @override
  String get demoSharedZAxisSavedRecipesListTitle => 'Recetas guardadas';

  @override
  String get demoSharedZAxisProfileSettingLabel => 'Perfil';

  @override
  String get demoSharedZAxisNotificationSettingLabel => 'Notificaciones';

  @override
  String get demoSharedZAxisPrivacySettingLabel => 'Privacidad';

  @override
  String get demoSharedZAxisHelpSettingLabel => 'Ayuda';

  @override
  String get demoFadeThroughTitle => 'Cambiar con atenuación';

  @override
  String get demoFadeThroughDescription => 'El patrón \"Cambiar con atenuación\" se utiliza en las transiciones entre elementos de la interfaz que no están estrechamente relacionados.';

  @override
  String get demoFadeThroughAlbumsDestination => 'Álbumes';

  @override
  String get demoFadeThroughPhotosDestination => 'Fotos';

  @override
  String get demoFadeThroughSearchDestination => 'Buscar';

  @override
  String get demoFadeThroughTextPlaceholder => '123 fotos';

  @override
  String get demoFadeScaleTitle => 'Atenuar';

  @override
  String get demoFadeScaleDescription => 'El patrón \"Atenuar\" se utiliza en elementos de la interfaz que se abren o se cierran dentro de los límites de la pantalla; por ejemplo, cuando un cuadro de diálogo se atenúa en el centro de la pantalla.';

  @override
  String get demoFadeScaleShowAlertDialogButton => 'VER MODAL';

  @override
  String get demoFadeScaleShowFabButton => 'VER BAF';

  @override
  String get demoFadeScaleHideFabButton => 'OCULTAR BAF';

  @override
  String get demoFadeScaleAlertDialogHeader => 'Cuadro de diálogo de alerta';

  @override
  String get demoFadeScaleAlertDialogCancelButton => 'CANCELAR';

  @override
  String get demoFadeScaleAlertDialogDiscardButton => 'DESCARTAR';

  @override
  String get demoColorsTitle => 'Colores';

  @override
  String get demoColorsSubtitle => 'Todos los colores predefinidos';

  @override
  String get demoColorsDescription => 'Color y muestra de color que representa la paleta de colores de Material Design.';

  @override
  String get demoTypographyTitle => 'Tipografía';

  @override
  String get demoTypographySubtitle => 'Todos los estilos de texto predefinidos';

  @override
  String get demoTypographyDescription => 'Las definiciones para los estilos tipográficos que se han encontrado en Material Design.';

  @override
  String get demo2dTransformationsTitle => 'Transformaciones a 2D';

  @override
  String get demo2dTransformationsSubtitle => 'Gira y usa zoom o el modo panorámico';

  @override
  String get demo2dTransformationsDescription => 'Toca un archivo para editarlo o usa gestos para desplazarte por la imagen. Arrastra el dedo para moverte por la imagen, pellízcala para hacer zoom y usa dos dedos para girarla. Pulsa el botón para revertir las transformaciones si quieres volver a la orientación inicial.';

  @override
  String get demo2dTransformationsResetTooltip => 'Revertir transformaciones';

  @override
  String get demo2dTransformationsEditTooltip => 'Editar mosaico';

  @override
  String get buttonText => 'BOTÓN';

  @override
  String get demoBottomSheetTitle => 'Hoja inferior';

  @override
  String get demoBottomSheetSubtitle => 'Hojas inferiores modales y persistentes';

  @override
  String get demoBottomSheetPersistentTitle => 'Hoja inferior persistente';

  @override
  String get demoBottomSheetPersistentDescription => 'Una hoja inferior persistente muestra información complementaria al contenido principal de la aplicación que estén utilizando y permanece siempre visible, aunque los usuarios interactúen con otras partes de la aplicación.';

  @override
  String get demoBottomSheetModalTitle => 'Hoja inferior modal';

  @override
  String get demoBottomSheetModalDescription => 'Una hoja inferior modal es la alternativa al menú o a los cuadros de diálogo y evita que los usuarios interactúen con el resto de la aplicación que estén utilizando.';

  @override
  String get demoBottomSheetAddLabel => 'Añadir';

  @override
  String get demoBottomSheetButtonText => 'MOSTRAR HOJA INFERIOR';

  @override
  String get demoBottomSheetHeader => 'Encabezado';

  @override
  String demoBottomSheetItem(Object value) {
    return 'Artículo ${value}';
  }

  @override
  String get demoListsTitle => 'Listas';

  @override
  String get demoListsSubtitle => 'Diseños de listas por las que se puede desplazar';

  @override
  String get demoListsDescription => 'Fila con un altura fija que por lo general incluye texto y un icono al principio o al final.';

  @override
  String get demoOneLineListsTitle => 'Una línea';

  @override
  String get demoTwoLineListsTitle => 'Dos líneas';

  @override
  String get demoListsSecondary => 'Texto secundario';

  @override
  String get demoProgressIndicatorTitle => 'Indicadores de progreso';

  @override
  String get demoProgressIndicatorSubtitle => 'Lineal, circular, indeterminado';

  @override
  String get demoCircularProgressIndicatorTitle => 'Indicador de progreso circular';

  @override
  String get demoCircularProgressIndicatorDescription => 'Un indicador de progreso circular de Material Design, que gira para indicar que la aplicación no está disponible.';

  @override
  String get demoLinearProgressIndicatorTitle => 'Indicador de progreso lineal';

  @override
  String get demoLinearProgressIndicatorDescription => 'Un indicador de progreso lineal de Material Design, también denominado \"barra de progreso\".';

  @override
  String get demoPickersTitle => 'Selectores';

  @override
  String get demoPickersSubtitle => 'Selección de fecha y hora';

  @override
  String get demoDatePickerTitle => 'Selector de fecha';

  @override
  String get demoDatePickerDescription => 'Muestra un cuadro de diálogo que contiene un selector de fecha de Material Design.';

  @override
  String get demoTimePickerTitle => 'Selector de hora';

  @override
  String get demoTimePickerDescription => 'Muestra un cuadro de diálogo que contiene un selector de hora de Material Design.';

  @override
  String get demoPickersShowPicker => 'MOSTRAR SELECTOR';

  @override
  String get demoTabsTitle => 'Pestañas';

  @override
  String get demoTabsScrollingTitle => 'Desplazamiento';

  @override
  String get demoTabsNonScrollingTitle => 'Sin desplazamiento';

  @override
  String get demoTabsSubtitle => 'Pestañas con vistas desplazables por separado';

  @override
  String get demoTabsDescription => 'En las pestañas se organiza contenido en distintas pantallas, conjuntos de datos y otras interacciones.';

  @override
  String get demoSnackbarsTitle => 'Snackbar';

  @override
  String get demoSnackbarsSubtitle => 'Los snackbar muestran mensajes en la parte inferior de la pantalla';

  @override
  String get demoSnackbarsDescription => 'Los snackbar proporcionan a los usuarios información sobre un proceso que una aplicación ha completado o va a iniciar. Aparecen de manera temporal en la parte inferior de la pantalla. No deberían interrumpir la experiencia de usuario, y desaparecen sin que los usuarios tengan que hacer nada.';

  @override
  String get demoSnackbarsButtonLabel => 'MOSTRAR UN SNACKBAR';

  @override
  String get demoSnackbarsText => 'Esto es un snackbar.';

  @override
  String get demoSnackbarsActionButtonLabel => 'ACCIÓN';

  @override
  String get demoSnackbarsAction => 'Has tocado la acción del snackbar.';

  @override
  String get demoSelectionControlsTitle => 'Controles de selección';

  @override
  String get demoSelectionControlsSubtitle => 'Casillas, botones de selección e interruptores';

  @override
  String get demoSelectionControlsCheckboxTitle => 'Casilla';

  @override
  String get demoSelectionControlsCheckboxDescription => 'Las casillas permiten que los usuarios seleccionen varias opciones de un conjunto de opciones. Por lo general, las casillas pueden tener dos valores (verdadero o falso), aunque hay casillas que pueden tener tres (el tercero es el valor nulo).';

  @override
  String get demoSelectionControlsRadioTitle => 'Botón de selección';

  @override
  String get demoSelectionControlsRadioDescription => 'Los botones de selección permiten que los usuarios seleccionen una opción de un conjunto de opciones. Utilízalos si quieres que los usuarios elijan una única opción, pero quieres mostrarles todas las que están disponibles.';

  @override
  String get demoSelectionControlsSwitchTitle => 'Interruptor';

  @override
  String get demoSelectionControlsSwitchDescription => 'Los interruptores controlan el estado de un solo ajuste. La etiqueta insertada del interruptor debería indicar de forma clara el ajuste que controla y el estado en el que está.';

  @override
  String get demoBottomTextFieldsTitle => 'Campos de texto';

  @override
  String get demoTextFieldTitle => 'Campos de texto';

  @override
  String get demoTextFieldSubtitle => 'Una línea de texto y números editables';

  @override
  String get demoTextFieldDescription => 'En los campos de texto, los usuarios pueden introducir texto en la interfaz. Estos campos suelen aparecer en formularios y cuadros de diálogo.';

  @override
  String get demoTextFieldShowPasswordLabel => 'Mostrar contraseña';

  @override
  String get demoTextFieldHidePasswordLabel => 'Ocultar contraseña';

  @override
  String get demoTextFieldFormErrors => 'Corrige los errores marcados en rojo antes de enviar el formulario.';

  @override
  String get demoTextFieldNameRequired => 'Es obligatorio indicar el nombre.';

  @override
  String get demoTextFieldOnlyAlphabeticalChars => 'Introduce solo caracteres alfabéticos.';

  @override
  String get demoTextFieldEnterUSPhoneNumber => '(###) ###-####. Introduce un número de teléfono de EE. UU.';

  @override
  String get demoTextFieldEnterPassword => 'Introduce una contraseña.';

  @override
  String get demoTextFieldPasswordsDoNotMatch => 'Las contraseñas no coinciden';

  @override
  String get demoTextFieldWhatDoPeopleCallYou => '¿Cómo te llamas?';

  @override
  String get demoTextFieldNameField => 'Nombre*';

  @override
  String get demoTextFieldWhereCanWeReachYou => '¿Cómo podemos ponernos en contacto contigo?';

  @override
  String get demoTextFieldPhoneNumber => 'Número de teléfono*';

  @override
  String get demoTextFieldYourEmailAddress => 'Tu dirección de correo electrónico';

  @override
  String get demoTextFieldEmail => 'Correo electrónico';

  @override
  String get demoTextFieldTellUsAboutYourself => 'Háblanos de ti (p. ej., dinos a qué te dedicas o las aficiones que tienes)';

  @override
  String get demoTextFieldKeepItShort => 'Sé breve, esto es solo una demostración.';

  @override
  String get demoTextFieldLifeStory => 'Biografía';

  @override
  String get demoTextFieldSalary => 'Salario';

  @override
  String get demoTextFieldUSD => 'USD';

  @override
  String get demoTextFieldNoMoreThan => 'Menos de 8 caracteres';

  @override
  String get demoTextFieldPassword => 'Contraseña*';

  @override
  String get demoTextFieldRetypePassword => 'Vuelve a escribir la contraseña*';

  @override
  String get demoTextFieldSubmit => 'ENVIAR';

  @override
  String demoTextFieldNameHasPhoneNumber(Object name, Object phoneNumber) {
    return 'El número de teléfono de ${name} es ${phoneNumber}';
  }

  @override
  String get demoTextFieldRequiredField => '* indica que el campo es obligatorio';

  @override
  String get demoTooltipTitle => 'Descripciones emergentes';

  @override
  String get demoTooltipSubtitle => 'Mensaje breve que se muestra al mantener pulsado o colocar el cursor sobre un elemento';

  @override
  String get demoTooltipDescription => 'Las descripciones emergentes proporcionan etiquetas de texto que ayudan a explicar la función de un botón u otras acciones de la interfaz de usuario. Las descripciones emergentes muestran texto informativo cuando los usuarios colocan el cursor sobre un elemento, se centran en él o lo mantienen pulsado.';

  @override
  String get demoTooltipInstructions => 'Mantén pulsado o coloca el cursor sobre un elemento para que se muestre la descripción emergente.';

  @override
  String get bottomNavigationCommentsTab => 'Comentarios';

  @override
  String get bottomNavigationCalendarTab => 'Calendario';

  @override
  String get bottomNavigationAccountTab => 'Cuenta';

  @override
  String get bottomNavigationAlarmTab => 'Alarma';

  @override
  String get bottomNavigationCameraTab => 'Cámara';

  @override
  String bottomNavigationContentPlaceholder(Object title) {
    return 'Marcador de posición de la pestaña ${title}';
  }

  @override
  String get buttonTextCreate => 'Crear';

  @override
  String dialogSelectedOption(Object value) {
    return 'Has seleccionado: \"${value}\"';
  }

  @override
  String get chipTurnOnLights => 'Encender las luces';

  @override
  String get chipSmall => 'Pequeño';

  @override
  String get chipMedium => 'Medio';

  @override
  String get chipLarge => 'Grande';

  @override
  String get chipElevator => 'Ascensor';

  @override
  String get chipWasher => 'Lavadora';

  @override
  String get chipFireplace => 'Chimenea';

  @override
  String get chipBiking => 'Bicicleta';

  @override
  String get dialogDiscardTitle => '¿Quieres descartar el borrador?';

  @override
  String get dialogLocationTitle => '¿Quieres utilizar el servicio de ubicación de Google?';

  @override
  String get dialogLocationDescription => 'Permite que Google ayude a las aplicaciones a determinar la ubicación haciendo que el usuario envíe datos de ubicación anónimos a Google aunque las aplicaciones no se estén ejecutando.';

  @override
  String get dialogCancel => 'CANCELAR';

  @override
  String get dialogDiscard => 'DESCARTAR';

  @override
  String get dialogDisagree => 'RECHAZAR';

  @override
  String get dialogAgree => 'ACEPTAR';

  @override
  String get dialogSetBackup => 'Crear cuenta de copia de seguridad';

  @override
  String get dialogAddAccount => 'Añadir cuenta';

  @override
  String get dialogShow => 'MOSTRAR CUADRO DE DIÁLOGO';

  @override
  String get dialogFullscreenTitle => 'Cuadro de diálogo de pantalla completa';

  @override
  String get dialogFullscreenSave => 'GUARDAR';

  @override
  String get dialogFullscreenDescription => 'Demostración del cuadro de diálogo de pantalla completa';

  @override
  String get cupertinoButton => 'Botón';

  @override
  String get cupertinoButtonWithBackground => 'Con fondo';

  @override
  String get cupertinoAlertCancel => 'Cancelar';

  @override
  String get cupertinoAlertDiscard => 'Descartar';

  @override
  String get cupertinoAlertLocationTitle => '¿Das permiso a Maps para que acceda a tu ubicación mientras usas la aplicación?';

  @override
  String get cupertinoAlertLocationDescription => 'Se mostrará tu ubicación en el mapa y se utilizará para ofrecerte indicaciones, resultados de búsqueda cercanos y la duración prevista de los desplazamientos.';

  @override
  String get cupertinoAlertAllow => 'Permitir';

  @override
  String get cupertinoAlertDontAllow => 'No permitir';

  @override
  String get cupertinoAlertFavoriteDessert => 'Seleccionar postre favorito';

  @override
  String get cupertinoAlertDessertDescription => 'En la siguiente lista, elige tu tipo de postre favorito. Lo que elijas se usará para personalizar la lista de restaurantes recomendados de tu zona.';

  @override
  String get cupertinoAlertCheesecake => 'Tarta de queso';

  @override
  String get cupertinoAlertTiramisu => 'Tiramisú';

  @override
  String get cupertinoAlertApplePie => 'Tarta de manzana';

  @override
  String get cupertinoAlertChocolateBrownie => 'Brownie de chocolate';

  @override
  String get cupertinoShowAlert => 'Mostrar alerta';

  @override
  String get colorsRed => 'ROJO';

  @override
  String get colorsPink => 'ROSA';

  @override
  String get colorsPurple => 'MORADO';

  @override
  String get colorsDeepPurple => 'VIOLETA INTENSO';

  @override
  String get colorsIndigo => 'ÍNDIGO';

  @override
  String get colorsBlue => 'AZUL';

  @override
  String get colorsLightBlue => 'AZUL CLARO';

  @override
  String get colorsCyan => 'CIAN';

  @override
  String get colorsTeal => 'TURQUESA';

  @override
  String get colorsGreen => 'VERDE';

  @override
  String get colorsLightGreen => 'VERDE CLARO';

  @override
  String get colorsLime => 'LIMA';

  @override
  String get colorsYellow => 'AMARILLO';

  @override
  String get colorsAmber => 'ÁMBAR';

  @override
  String get colorsOrange => 'NARANJA';

  @override
  String get colorsDeepOrange => 'NARANJA INTENSO';

  @override
  String get colorsBrown => 'MARRÓN';

  @override
  String get colorsGrey => 'GRIS';

  @override
  String get colorsBlueGrey => 'GRIS AZULADO';

  @override
  String get placeChennai => 'Chennai';

  @override
  String get placeTanjore => 'Thanjavur';

  @override
  String get placeChettinad => 'Chettinad';

  @override
  String get placePondicherry => 'Puducherry';

  @override
  String get placeFlowerMarket => 'Mercado de Flores';

  @override
  String get placeBronzeWorks => 'Obras de bronce';

  @override
  String get placeMarket => 'Mercado';

  @override
  String get placeThanjavurTemple => 'Templo de Thanjavur';

  @override
  String get placeSaltFarm => 'Salina';

  @override
  String get placeScooters => 'Scooters';

  @override
  String get placeSilkMaker => 'Fabricante de seda';

  @override
  String get placeLunchPrep => 'Preparación de la comida';

  @override
  String get placeBeach => 'Playa';

  @override
  String get placeFisherman => 'Pescador';

  @override
  String get starterAppTitle => 'Aplicación de inicio';

  @override
  String get starterAppDescription => 'Diseño de inicio adaptable';

  @override
  String get starterAppGenericButton => 'BOTÓN';

  @override
  String get starterAppTooltipAdd => 'Añadir';

  @override
  String get starterAppTooltipFavorite => 'Favorito';

  @override
  String get starterAppTooltipShare => 'Compartir';

  @override
  String get starterAppTooltipSearch => 'Buscar';

  @override
  String get starterAppGenericTitle => 'Título';

  @override
  String get starterAppGenericSubtitle => 'Subtítulo';

  @override
  String get starterAppGenericHeadline => 'Título';

  @override
  String get starterAppGenericBody => 'Cuerpo';

  @override
  String starterAppDrawerItem(Object value) {
    return 'Artículo ${value}';
  }

  @override
  String get shrineMenuCaption => 'MENÚ';

  @override
  String get shrineCategoryNameAll => 'TODO';

  @override
  String get shrineCategoryNameAccessories => 'ACCESORIOS';

  @override
  String get shrineCategoryNameClothing => 'ROPA';

  @override
  String get shrineCategoryNameHome => 'CASA';

  @override
  String get shrineLogoutButtonCaption => 'CERRAR SESIÓN';

  @override
  String get shrineLoginUsernameLabel => 'Nombre de usuario';

  @override
  String get shrineLoginPasswordLabel => 'Contraseña';

  @override
  String get shrineCancelButtonCaption => 'CANCELAR';

  @override
  String get shrineNextButtonCaption => 'SIGUIENTE';

  @override
  String get shrineCartPageCaption => 'CARRITO';

  @override
  String shrineProductQuantity(Object quantity) {
    return 'Cantidad: ${quantity}';
  }

  @override
  String shrineProductPrice(Object price) {
    return 'x ${price}';
  }

  @override
  String shrineCartItemCount(int quantity) {
    return intl.Intl.pluralLogic(
      quantity,
      locale: localeName,
      zero: 'NO HAY ELEMENTOS',
      one: '1 ELEMENTO',
      other: '${quantity} ELEMENTOS',
    );
  }

  @override
  String get shrineCartClearButtonCaption => 'VACIAR CARRITO';

  @override
  String get shrineCartTotalCaption => 'TOTAL';

  @override
  String get shrineCartSubtotalCaption => 'Subtotal:';

  @override
  String get shrineCartShippingCaption => 'Gastos de envío:';

  @override
  String get shrineCartTaxCaption => 'Impuestos:';

  @override
  String get shrineProductVagabondSack => 'Mochila Vagabond';

  @override
  String get shrineProductStellaSunglasses => 'Gafas de sol Stella';

  @override
  String get shrineProductWhitneyBelt => 'Cinturón Whitney';

  @override
  String get shrineProductGardenStrand => 'Collar de cuentas';

  @override
  String get shrineProductStrutEarrings => 'Pendientes Strut';

  @override
  String get shrineProductVarsitySocks => 'Calcetines Varsity';

  @override
  String get shrineProductWeaveKeyring => 'Llavero de tela';

  @override
  String get shrineProductGatsbyHat => 'Gorra';

  @override
  String get shrineProductShrugBag => 'Mochila Shrug';

  @override
  String get shrineProductGiltDeskTrio => 'Conjunto de tres mesas';

  @override
  String get shrineProductCopperWireRack => 'Estantería de alambre de cobre';

  @override
  String get shrineProductSootheCeramicSet => 'Juego de tazas para infusiones';

  @override
  String get shrineProductHurrahsTeaSet => 'Juego de té clásico';

  @override
  String get shrineProductBlueStoneMug => 'Taza Blue Stone';

  @override
  String get shrineProductRainwaterTray => 'Cubo de recogida de agua de lluvia';

  @override
  String get shrineProductChambrayNapkins => 'Servilletas de cambray';

  @override
  String get shrineProductSucculentPlanters => 'Maceteros para plantas suculentas';

  @override
  String get shrineProductQuartetTable => 'Mesa cuadrada';

  @override
  String get shrineProductKitchenQuattro => 'Kitchen Quattro';

  @override
  String get shrineProductClaySweater => 'Jersey Clay';

  @override
  String get shrineProductSeaTunic => 'Túnica azul claro';

  @override
  String get shrineProductPlasterTunic => 'Túnica color yeso';

  @override
  String get shrineProductWhitePinstripeShirt => 'Camisa blanca de rayas diplomáticas';

  @override
  String get shrineProductChambrayShirt => 'Camisa de cambray';

  @override
  String get shrineProductSeabreezeSweater => 'Jersey de tejido liviano';

  @override
  String get shrineProductGentryJacket => 'Chaqueta Gentry';

  @override
  String get shrineProductNavyTrousers => 'Pantalones azul marino';

  @override
  String get shrineProductWalterHenleyWhite => 'Camiseta de rayas (blanca)';

  @override
  String get shrineProductSurfAndPerfShirt => 'Camisa surfera';

  @override
  String get shrineProductGingerScarf => 'Bufanda anaranjada';

  @override
  String get shrineProductRamonaCrossover => 'Blusa cruzada Ramona';

  @override
  String get shrineProductClassicWhiteCollar => 'Camisa de cuello clásico en blanco';

  @override
  String get shrineProductCeriseScallopTee => 'Camiseta color cereza';

  @override
  String get shrineProductShoulderRollsTee => 'Camiseta de hombros descubiertos';

  @override
  String get shrineProductGreySlouchTank => 'Camiseta de tirantes gris';

  @override
  String get shrineProductSunshirtDress => 'Vestido playero';

  @override
  String get shrineProductFineLinesTee => 'Camiseta de rayas finas';

  @override
  String get shrineTooltipSearch => 'Buscar';

  @override
  String get shrineTooltipSettings => 'Ajustes';

  @override
  String get shrineTooltipOpenMenu => 'Abrir menú';

  @override
  String get shrineTooltipCloseMenu => 'Cerrar menú';

  @override
  String get shrineTooltipCloseCart => 'Cerrar carrito';

  @override
  String shrineScreenReaderCart(int quantity) {
    return intl.Intl.pluralLogic(
      quantity,
      locale: localeName,
      zero: 'Carrito: 0 artículos',
      one: 'Carrito: 1 artículo',
      other: 'Carrito: ${quantity} artículos',
    );
  }

  @override
  String get shrineScreenReaderProductAddToCart => 'Añadir al carrito';

  @override
  String shrineScreenReaderRemoveProductButton(Object product) {
    return 'Quitar ${product}';
  }

  @override
  String get shrineTooltipRemoveItem => 'Quitar elemento';

  @override
  String get craneFormDiners => 'Restaurantes';

  @override
  String get craneFormDate => 'Seleccionar fecha';

  @override
  String get craneFormTime => 'Seleccionar hora';

  @override
  String get craneFormLocation => 'Seleccionar ubicación';

  @override
  String get craneFormTravelers => 'Viajeros';

  @override
  String get craneFormOrigin => 'Elegir origen';

  @override
  String get craneFormDestination => 'Elegir destino';

  @override
  String get craneFormDates => 'Seleccionar fechas';

  @override
  String craneHours(int hours) {
    return intl.Intl.pluralLogic(
      hours,
      locale: localeName,
      one: '1 h',
      other: '${hours} h',
    );
  }

  @override
  String craneMinutes(int minutes) {
    return intl.Intl.pluralLogic(
      minutes,
      locale: localeName,
      one: '1 min',
      other: '${minutes} min',
    );
  }

  @override
  String craneFlightDuration(Object hoursShortForm, Object minutesShortForm) {
    return '${hoursShortForm} ${minutesShortForm}';
  }

  @override
  String get craneFly => 'VOLAR';

  @override
  String get craneSleep => 'DORMIR';

  @override
  String get craneEat => 'COMER';

  @override
  String get craneFlySubhead => 'Buscar vuelos por destino';

  @override
  String get craneSleepSubhead => 'Buscar propiedades por destino';

  @override
  String get craneEatSubhead => 'Buscar restaurantes por destino';

  @override
  String craneFlyStops(int numberOfStops) {
    return intl.Intl.pluralLogic(
      numberOfStops,
      locale: localeName,
      zero: 'Vuelo directo',
      one: '1 escala',
      other: '${numberOfStops} escalas',
    );
  }

  @override
  String craneSleepProperties(int totalProperties) {
    return intl.Intl.pluralLogic(
      totalProperties,
      locale: localeName,
      zero: 'No hay propiedades disponibles',
      one: '1 propiedad disponible',
      other: '${totalProperties} propiedades disponibles',
    );
  }

  @override
  String craneEatRestaurants(int totalRestaurants) {
    return intl.Intl.pluralLogic(
      totalRestaurants,
      locale: localeName,
      zero: 'No hay restaurantes',
      one: '1 restaurante',
      other: '${totalRestaurants} restaurantes',
    );
  }

  @override
  String get craneFly0 => 'Aspen (Estados Unidos)';

  @override
  String get craneFly1 => 'Big Sur (Estados Unidos)';

  @override
  String get craneFly2 => 'Valle del Khumbu (Nepal)';

  @override
  String get craneFly3 => 'Machu Picchu (Perú)';

  @override
  String get craneFly4 => 'Malé (Maldivas)';

  @override
  String get craneFly5 => 'Vitznau (Suiza)';

  @override
  String get craneFly6 => 'Ciudad de México (México)';

  @override
  String get craneFly7 => 'Monte Rushmore (Estados Unidos)';

  @override
  String get craneFly8 => 'Singapur';

  @override
  String get craneFly9 => 'La Habana (Cuba)';

  @override
  String get craneFly10 => 'El Cairo (Egipto)';

  @override
  String get craneFly11 => 'Lisboa (Portugal)';

  @override
  String get craneFly12 => 'Napa (Estados Unidos)';

  @override
  String get craneFly13 => 'Bali (Indonesia)';

  @override
  String get craneSleep0 => 'Malé (Maldivas)';

  @override
  String get craneSleep1 => 'Aspen (Estados Unidos)';

  @override
  String get craneSleep2 => 'Machu Picchu (Perú)';

  @override
  String get craneSleep3 => 'La Habana (Cuba)';

  @override
  String get craneSleep4 => 'Vitznau (Suiza)';

  @override
  String get craneSleep5 => 'Big Sur (Estados Unidos)';

  @override
  String get craneSleep6 => 'Napa (Estados Unidos)';

  @override
  String get craneSleep7 => 'Oporto (Portugal)';

  @override
  String get craneSleep8 => 'Tulum (México)';

  @override
  String get craneSleep9 => 'Lisboa (Portugal)';

  @override
  String get craneSleep10 => 'El Cairo (Egipto)';

  @override
  String get craneSleep11 => 'Taipéi (Taiwán)';

  @override
  String get craneEat0 => 'Nápoles (Italia)';

  @override
  String get craneEat1 => 'Dallas (Estados Unidos)';

  @override
  String get craneEat2 => 'Córdoba (Argentina)';

  @override
  String get craneEat3 => 'Portland (Estados Unidos)';

  @override
  String get craneEat4 => 'París (Francia)';

  @override
  String get craneEat5 => 'Seúl (Corea del Sur)';

  @override
  String get craneEat6 => 'Seattle (Estados Unidos)';

  @override
  String get craneEat7 => 'Nashville (Estados Unidos)';

  @override
  String get craneEat8 => 'Atlanta (Estados Unidos)';

  @override
  String get craneEat9 => 'Madrid (España)';

  @override
  String get craneEat10 => 'Lisboa (Portugal)';

  @override
  String get craneFly0SemanticLabel => 'Chalet en un paisaje nevado con árboles de hoja perenne';

  @override
  String get craneFly1SemanticLabel => 'Tienda de campaña en un campo';

  @override
  String get craneFly2SemanticLabel => 'Banderas de plegaria frente a una montaña nevada';

  @override
  String get craneFly3SemanticLabel => 'Ciudadela de Machu Picchu';

  @override
  String get craneFly4SemanticLabel => 'Bungalós flotantes';

  @override
  String get craneFly5SemanticLabel => 'Hotel a orillas de un lago y frente a montañas';

  @override
  String get craneFly6SemanticLabel => 'Vista aérea del Palacio de Bellas Artes';

  @override
  String get craneFly7SemanticLabel => 'Monte Rushmore';

  @override
  String get craneFly8SemanticLabel => 'Supertree Grove';

  @override
  String get craneFly9SemanticLabel => 'Hombre apoyado en un coche azul antiguo';

  @override
  String get craneFly10SemanticLabel => 'Minaretes de la mezquita de al-Azhar al atardecer';

  @override
  String get craneFly11SemanticLabel => 'Faro de ladrillos junto al mar';

  @override
  String get craneFly12SemanticLabel => 'Piscina con palmeras';

  @override
  String get craneFly13SemanticLabel => 'Piscina junto al mar con palmeras';

  @override
  String get craneSleep0SemanticLabel => 'Bungalós flotantes';

  @override
  String get craneSleep1SemanticLabel => 'Chalet en un paisaje nevado con árboles de hoja perenne';

  @override
  String get craneSleep2SemanticLabel => 'Ciudadela de Machu Picchu';

  @override
  String get craneSleep3SemanticLabel => 'Hombre apoyado en un coche azul antiguo';

  @override
  String get craneSleep4SemanticLabel => 'Hotel a orillas de un lago y frente a montañas';

  @override
  String get craneSleep5SemanticLabel => 'Tienda de campaña en un campo';

  @override
  String get craneSleep6SemanticLabel => 'Piscina con palmeras';

  @override
  String get craneSleep7SemanticLabel => 'Apartamentos de vivos colores en la Plaza de la Ribeira';

  @override
  String get craneSleep8SemanticLabel => 'Ruinas mayas en lo alto de un acantilado junto a una playa';

  @override
  String get craneSleep9SemanticLabel => 'Faro de ladrillos junto al mar';

  @override
  String get craneSleep10SemanticLabel => 'Minaretes de la mezquita de al-Azhar al atardecer';

  @override
  String get craneSleep11SemanticLabel => 'Rascacielos Taipei 101';

  @override
  String get craneEat0SemanticLabel => 'Pizza en un horno de leña';

  @override
  String get craneEat1SemanticLabel => 'Bar vacío con taburetes junto a la barra';

  @override
  String get craneEat2SemanticLabel => 'Hamburguesa';

  @override
  String get craneEat3SemanticLabel => 'Taco coreano';

  @override
  String get craneEat4SemanticLabel => 'Postre de chocolate';

  @override
  String get craneEat5SemanticLabel => 'Sala de un restaurante artístico';

  @override
  String get craneEat6SemanticLabel => 'Plato de gambas';

  @override
  String get craneEat7SemanticLabel => 'Entrada de una panadería';

  @override
  String get craneEat8SemanticLabel => 'Plato con cangrejos de río';

  @override
  String get craneEat9SemanticLabel => 'Mostrador de cafetería con pastas';

  @override
  String get craneEat10SemanticLabel => 'Mujer que sostiene un gran sándwich de pastrami';

  @override
  String get fortnightlyMenuFrontPage => 'Portada';

  @override
  String get fortnightlyMenuWorld => 'Todo el mundo';

  @override
  String get fortnightlyMenuUS => 'EE. UU.';

  @override
  String get fortnightlyMenuPolitics => 'Política';

  @override
  String get fortnightlyMenuBusiness => 'Empresa';

  @override
  String get fortnightlyMenuTech => 'Tecnología';

  @override
  String get fortnightlyMenuScience => 'Ciencia';

  @override
  String get fortnightlyMenuSports => 'Deportes';

  @override
  String get fortnightlyMenuTravel => 'Viajes';

  @override
  String get fortnightlyMenuCulture => 'Cultura';

  @override
  String get fortnightlyTrendingTechDesign => 'DiseñoTecnológico';

  @override
  String get fortnightlyTrendingReform => 'Reforma';

  @override
  String get fortnightlyTrendingHealthcareRevolution => 'RevoluciónAsistenciaSanitaria';

  @override
  String get fortnightlyTrendingGreenArmy => 'EjércitoVerde';

  @override
  String get fortnightlyTrendingStocks => 'Bolsa';

  @override
  String get fortnightlyLatestUpdates => 'Últimas novedades';

  @override
  String get fortnightlyHeadlineHealthcare => 'La silenciosa pero poderosa revolución de la asistencia sanitaria';

  @override
  String get fortnightlyHeadlineWar => 'Vidas americanas divididas por la guerra';

  @override
  String get fortnightlyHeadlineGasoline => 'El futuro de la gasolina';

  @override
  String get fortnightlyHeadlineArmy => 'Reformar el Ejército Verde desde dentro';

  @override
  String get fortnightlyHeadlineStocks => 'A medida que la bolsa se estanca, muchos vuelven su mirada a la moneda';

  @override
  String get fortnightlyHeadlineFabrics => 'Diseñadores utilizan la tecnología para fabricar tejidos futuristas';

  @override
  String get fortnightlyHeadlineFeminists => 'Las feministas se enfrentan al partidismo';

  @override
  String get fortnightlyHeadlineBees => 'Escasez de granjas de abejas';

  @override
  String get replyInboxLabel => 'Inbox';

  @override
  String get replyStarredLabel => 'Starred';

  @override
  String get replySentLabel => 'Sent';

  @override
  String get replyTrashLabel => 'Trash';

  @override
  String get replySpamLabel => 'Spam';

  @override
  String get replyDraftsLabel => 'Drafts';
}
