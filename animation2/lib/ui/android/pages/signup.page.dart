import 'package:animation1/ui/android/pages/submit-form.page.dart';
import 'package:flutter/material.dart';
import 'package:fluttie/fluttie.dart';

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  var busy = false;
  var done = false;
  FluttieAnimationController animationCtrl;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    prepareAnimation();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
    animationCtrl?.dispose();
  }

  prepareAnimation() async {
    var instance = Fluttie();

    var checkAnimation =
        await instance.loadAnimationFromAsset("assets/done.json");

    animationCtrl = await instance.prepareAnimation(
      checkAnimation,
      duration: Duration(seconds: 2),
      repeatCount: RepeatCount.dontRepeat(),
      repeatMode: RepeatMode.START_OVER,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: double.infinity,
            alignment: Alignment.center,
          ),
          FluttieAnimation(animationCtrl),
          !done
              ? SubmitForm(
                  busy: busy,
                  func: submit,
                )
              : Container(),
        ],
      ),
    );
  }

  Future<Function> submit() async {
    setState(() {
      busy = true;
    });

    Future.delayed(
      Duration(seconds: 4),
      () => setState(
        () {
          done = true;
          animationCtrl.start();
        },
      ),
    );
  }
}
