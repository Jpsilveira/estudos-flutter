import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shopping',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Container(
          color: Colors.red,
          width: double.infinity,
          child: ListView(
            // mainAxisAlignment: MainAxisAlignment.spaceAround,
            // crossAxisAlignment: CrossAxisAlignment.center,
            scrollDirection: Axis.vertical,
            children: [
              Container(
                color: Colors.yellow,
                child: Text(
                  "Item 0",
                ),
              ),
              Text("Item 1"),
              Text("Item 2"),
              Text("Item 3"),
              Text("Item 4"),
              Column(
                children: [
                  Text("C1"),
                  Text("C2"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                  Text("C3"),
                ],
              )
            ],
          ),
        ),
      ),
      appBar: AppBar(
        title: Center(
          child: Text(
            "Bar",
          ),
        ),
      ),
    );
  }
}
